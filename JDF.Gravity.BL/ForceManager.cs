﻿using System;
using System.Collections.Generic;
using System.Linq;
using JDF.Gravity.BL.Models;
using JDF.Gravity.PL;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace JDF.Gravity.BL
{
    public static class ForceManager
    {
        /**********************************************************************************
        /* Name: Load
        /* Description: Loads all force entries from database
        /*********************************************************************************/
        public static List<Force> Load()
        {
            try
            {
                List<Force> forces = new List<Force>();

                using (GravityEntities dc = new GravityEntities())
                {
                    // Get all rows and add a new Force object for each
                    dc.tblGravities.ToList().ForEach(g => forces.Add(new Force
                    {
                        Id = g.Id,
                        ChangeDate = g.ChangeDate,
                        Mass1 = (double)g.Mass1,
                        Mass2 = (double)g.Mass2,
                        Distance = (double)g.Distance,
                        ForceOfGravity = (double)g.Force
                    }));
                    return forces;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: LoadById
        /* Description: Load force entry from database for a specified ID
        /*********************************************************************************/
        public static Force LoadById(Guid id)
        {
            try
            {
                Force force = new Force();

                using (GravityEntities dc = new GravityEntities())
                {
                    // Get the row
                    var tblGravity = dc.tblGravities.FirstOrDefault(g => g.Id == id);

                    // Get the row values
                    force.Id = tblGravity.Id;
                    force.ChangeDate = tblGravity.ChangeDate;
                    force.Mass1 = (double)tblGravity.Mass1;
                    force.Mass2 = (double)tblGravity.Mass2;
                    force.Distance = (double)tblGravity.Distance;
                    force.ForceOfGravity = (double)tblGravity.Force;

                    return force;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: Insert
        /* Description: Inserts a new force entry into the database
        /*********************************************************************************/
        public static int Insert(Force force, bool rollback = false)
        {
            IDbContextTransaction transaction = null;

            try
            {
                using (GravityEntities dc = new GravityEntities())
                {
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    tblGravity tblGravity = new tblGravity();

                    // Set the values
                    tblGravity.Id = Guid.NewGuid(); // Get a new ID
                    tblGravity.ChangeDate = DateTime.Now; // Use now for entry time
                    tblGravity.Mass1 = force.Mass1;
                    tblGravity.Mass2 = force.Mass2;
                    tblGravity.Distance = force.Distance;
                    tblGravity.Force = force.ForceOfGravity;

                    force.Id = tblGravity.Id; // Backfill ID to keep in sync

                    dc.tblGravities.Add(tblGravity); // Add the row
                    int results = dc.SaveChanges();

                    if (rollback) transaction.Rollback();

                    return results;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: Update
        /* Description: Updates a force entry in the database
        /*********************************************************************************/
        public static int Update(Force force, bool rollback = false)
        {
            IDbContextTransaction transaction = null;

            try
            {
                using (GravityEntities dc = new GravityEntities())
                {
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    // Get the row
                    tblGravity tblGravity = dc.tblGravities.FirstOrDefault(g => g.Id == force.Id);

                    if (tblGravity != null)
                    {
                        // Update the values
                        tblGravity.ChangeDate = DateTime.Now; // Use now for new change time
                        tblGravity.Mass1 = force.Mass1;
                        tblGravity.Mass2 = force.Mass2;
                        tblGravity.Distance = force.Distance;
                        tblGravity.Force = force.ForceOfGravity;

                        int results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results; 
                    }
                    else
                    {
                        throw new Exception("Row does not exist.");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: Delete
        /* Description: Deletes a force entry from the database
        /*********************************************************************************/
        public static int Delete(Guid id, bool rollback = false)
        {
            IDbContextTransaction transaction = null;

            try
            {
                using (GravityEntities dc = new GravityEntities())
                {
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    // Get the row
                    tblGravity tblGravity = dc.tblGravities.FirstOrDefault(g => g.Id == id);

                    if (tblGravity != null)
                    {
                        dc.tblGravities.Remove(tblGravity); // Remove the row
                        int results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row does not exist.");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: CalculateForce
        /* Description: Calculates force value from database stored procedure
        /*********************************************************************************/
        private static double CalculateForce(double mass1, double mass2, double distance)
        {
            try
            {
                // Set parameters
                using (GravityEntities dc = new GravityEntities())
                {
                    var parameterMass1 = new SqlParameter
                    {
                        ParameterName = "Mass1",
                        SqlDbType = System.Data.SqlDbType.Float,
                        Value = mass1
                    };

                    var parameterMass2 = new SqlParameter
                    {
                        ParameterName = "Mass2",
                        SqlDbType = System.Data.SqlDbType.Float,
                        Value = mass2
                    };

                    var parameterDistance = new SqlParameter
                    {
                        ParameterName = "Distance",
                        SqlDbType = System.Data.SqlDbType.Float,
                        Value = distance
                    };

                    // Return the value
                    return (double)dc.Set<spCalculateForce>().FromSqlRaw("exec spCalculateForce @Mass1, @Mass2, @Distance", parameterMass1, parameterMass2, parameterDistance).ToList().FirstOrDefault().Force;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**********************************************************************************
        /* Name: SetForce
        /* Description: Sets the calculated force value for a given force entry
        /*********************************************************************************/
        public static void SetForce(Force force)
        {
            try
            {
                force.ForceOfGravity = CalculateForce(force.Mass1, force.Mass2, force.Distance);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
