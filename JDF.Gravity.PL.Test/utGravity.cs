using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace JDF.Gravity.PL.Test
{
    [TestClass]
    public class utGravity
    {
        protected GravityEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize() // Set up new data context and transaction before every test
        {
            dc = new GravityEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanup() // Rollback and clean up after every test
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            int expected = 1;

            int actual = dc.tblGravities.Count(); // Number of rows in the table
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InsertTest()
        {
            int expected = 1;

            tblGravity row = new tblGravity();
            row.Id = Guid.NewGuid();
            row.ChangeDate = DateTime.Now;
            row.Mass1 = 900;
            row.Mass2 = 1700;
            row.Distance = 20;
            row.Force = 50;

            dc.tblGravities.Add(row);
            int actual = dc.SaveChanges();

            Assert.AreEqual(expected, actual);
        }
    }
}
