﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JDF.Gravity.PL
{
    public partial class tblGravity
    {
        public Guid Id { get; set; }
        public DateTime ChangeDate { get; set; }
        public double Mass1 { get; set; }
        public double Mass2 { get; set; }
        public double Distance { get; set; }
        public double Force { get; set; }
    }
}
