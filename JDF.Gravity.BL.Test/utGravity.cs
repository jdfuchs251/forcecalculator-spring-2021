using JDF.Gravity.BL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace JDF.Gravity.BL.Test
{
    [TestClass]
    public class utGravity
    {
        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(1, ForceManager.Load().Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            int results = ForceManager.Insert(new Force
            {
                Id = Guid.Empty,
                ChangeDate = DateTime.Now,
                Mass1 = 10,
                Mass2 = 20,
                Distance = 30,
                ForceOfGravity = 40
            }, true);

            Assert.AreEqual(1, results);
        }

        [TestMethod]
        public void CalculateTest()
        {
            Force force = new Force();
            force.Mass1 = 9000000;
            force.Mass2 = 70000000;
            force.Distance = 20;
            ForceManager.SetForce(force);

            Assert.IsTrue(force.ForceOfGravity > 0);
        }
    }
}
