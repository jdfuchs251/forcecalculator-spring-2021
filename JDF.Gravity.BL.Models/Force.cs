﻿using System;

namespace JDF.Gravity.BL.Models
{
    public class Force
    {
        public Guid Id { get; set; }
        public DateTime ChangeDate { get; set; }
        public double Mass1 { get; set; }
        public double Mass2 { get; set; }
        public double Distance { get; set; }
        public double ForceOfGravity { get; set; }
    }
}
