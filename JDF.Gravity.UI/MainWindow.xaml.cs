﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JDF.Gravity.BL;
using JDF.Gravity.BL.Models;

namespace JDF.Gravity.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /**********************************************************************************
        /* Name: btnCalculate_Click
        /* Description: Displays the calculated force value and saves the calculation to the database
        /*********************************************************************************/
        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double mass1, mass2, distance;

                ValidateTextbox(txtMass1, out mass1);
                ValidateTextbox(txtMass2, out mass2);
                ValidateTextbox(txtDistance, out distance);

                Force force = new Force { Mass1 = mass1, Mass2 = mass2, Distance = distance };
                ForceManager.SetForce(force);

                ForceManager.Insert(force);

                lblForce.Content = "Force = " + force.ForceOfGravity.ToString();
            }
            catch (Exception ex)
            {
                lblStatus.Content = ex.Message;
            }
        }

        /**********************************************************************************
        /* Name: textbox_TextChanged
        /* Description: Clears out labels when the text in any textbox changes
        /*********************************************************************************/
        private void textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblForce.Content = string.Empty;
            lblStatus.Content = string.Empty;
        }

        /**********************************************************************************
        /* Name: ValidateTextbox
        /* Description: Validates that a textbox contains a valid double.  If valid, the value is returned as an output parameter
        /*********************************************************************************/
        private void ValidateTextbox(TextBox textBox, out double value)
        {
            try
            {
                if (!double.TryParse(textBox.Text, out value))
                {
                    textBox.Focus();
                    textBox.SelectAll();
                    throw new Exception("Please enter a number.");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
