﻿CREATE TABLE [dbo].[tblGravity]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ChangeDate] DATETIME NOT NULL, 
    [Mass1] FLOAT NOT NULL, 
    [Mass2] FLOAT NOT NULL, 
    [Distance] FLOAT NOT NULL, 
    [Force] FLOAT NOT NULL
)
