﻿CREATE PROCEDURE [dbo].[spCalculateForce]
	@mass1 float,
	@mass2 float,
	@distance float
AS
	DECLARE @g float = 6.67E-11
	IF @distance = 0
		BEGIN
			SELECT 0 as Force
		END
	ELSE
		BEGIN
			SELECT (@g * @mass1 * @mass2) / POWER(@distance, 2) as Force
		END
RETURN 0